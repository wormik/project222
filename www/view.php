<?php
/**
 * This script displays greeting to user and
 * his article with title
 */
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="css/style.css" type="text/css">
		<link rel="shortcut icon" href="http://sstatic.net/stackoverflow/img/favicon.ico">
		<title>Project222 | <?php echo $userName; ?></title>
	</head>
	<body>
		<div class="box">
			<?php
			/**
			 * @var User
			 */
			$user = $_SESSION['user'];
			if ($user != null) {
				echo 'Hello, '. $user->getName();;
				if ($user->getNewsTitle() === '' &&
					$user->getNewsArticle() === '') {
					$
					$title = 'You have no news';
					$article = '<a href=index.php?page=add_item.php>Add news</a>';
				}
				echo '
					<div class=post>
						<div class=title>'.$user->getNewsTitle().'</div>
						<div class=article>
							<p>'.$user->getNewsArticle().'</p>
						</div>
						<hr />
					</div>
					<form action="logout.php"> 
						<input type="submit" value="Logout" />
					</form>
				';
			} else {
				echo 'something wrong <br />';
				echo '<a href=index.php?page=login>Back to login</a>';
			}
			?>
		</div>   
	</body>
</html>
