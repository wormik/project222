<?php
/**
 * Entry page of application
 * 
 * Contains Login form
 * Checks entered username && password
 * in case of success redirects on view.php
 *
 */
require_once 'classes/UserDao.php';
require_once 'classes/User.php';
session_start();

if (!isset($_GET['page'])) {
	if (isset($_SESSION['user_id'])) {
		header('Location: index.php?page=view');
	} else {
		header('Location: index.php?page=login');
	}
}

require_once $_GET['page'].'.php';