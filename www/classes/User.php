<?php

/**
 * Logs user in session on construction
 * works with User and News data
 * can logout
 *
 * @author WormiK
 */
class User {
	private $id;
	private $name;
	private $newsTitle;
	private $newsArticle;
	
	public function __construct($id, $name, $title, $article) {
		$this->id = $id;
		$this->name = $name;
		$this->newsTitle = $title;
		$this->newsArticle = $article;
		$_SESSION['user'] = $this;
	}
	
	public function getId() {
		return $this->id;
	}

	public function getName() {
		return $this->name;
	}
	
	public function getNewsTitle() {
		return $this->newsTitle;
	}

	public function getNewsArticle() {
		return $this->newsArticle;
	}
}
