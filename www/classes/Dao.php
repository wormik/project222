<?php

/**
 * contains only class Dao
 * 
 * @version 0.2
 * @author Artem Bykov <earthworm@i.ua>
 */

/**
 * Data Access Object class, works with database
 * 
 * Connects to DB, can execute prepared statements
 */
class Dao 
{
	/**
	 * @var PDO DBH variable 
	 */
	private $db;

	/**
	 * @var string server path
	 */
	private $db_host;

	/**
	 * @var string username 
	 */
	private $db_user;

	/**
	 * @var string password 
	 */
	private $db_pass;

	/**
	 * @var string data base name
	 */
	private $db_name;

	/**
	 * @var boolean
	 */
	private $connected = false;

	/**
	 * Constructor
	 * 
	 * @param string $db_user Data Base username
	 * @param string $db_pass Data Base user's password
	 * @param string $db_name Data Base name
	 * @param string $db_host Data Base server path
	 */
	public function __construct(
	$db_user = 'root', $db_pass = '', $db_name = 'project222', $db_host = 'localhost'
	) {
		$this->db_host = $db_host;
		$this->db_user = $db_user;
		$this->db_pass = $db_pass;
		$this->db_name = $db_name;

		$this->connect();
	}

	/**
	 * Destructor
	 * 
	 * Calls disconnect() method which unsets DBH variable
	 */
	public function __destruct() 
	{
		$this->disconnect();
	}

	/**
	 * Connect method
	 * 
	 * Creates PHP Data Object
	 * Default server name, path, username, password
	 * 
	 * @return boolean | string exception message
	 */
	private function connect() 
	{
		if (!$this->connected) {

			/**
			 * try to connect to DB server on my localhost
			 * 'mysql:host=localhost;dbname=project222', root, '' 
			 */
			try {
				$this->db = new PDO(
						'mysql:host='.$this->db_host.';dbname='.$this->db_name, $this->db_user, $this->db_pass
				);

				$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				
				/**
				 * if not error cathed, must be connected already
				 */
				$this->connected = true;
				return true;
			} catch (PDOException $e) {
				return $e->getMessage();
			}
		} else {
			return true;
		}
	}

	/**
	 * Disconnect function
	 * 
	 * Unsets PDO variable
	 * 
	 * @return void
	 */
	private function disconnect() 
	{
		if ($this->connected) {
			unset($this->db);
			$this->connected = false;
		}
	}

	/**
	 * Executes query 
	 * 
	 * @deprecated since version 0.2
	 * 
	 * @todo ненужная функция. это всё можно сделать настройкой PDO. то же самое и про другие подобные функции из этого класса. посмотри, как настраивается pdo
	 * @param string $sql MySQL query
	 * @return boolean
	 */
	public function executeQuery($sql) 
	{
		$raw_result = $this->db->query($sql);
		if (!$raw_result) {
			die('DB Access error: ' . $this->db->errorInfo());
		}
		return true;
	}

	/**
	 * Executes query and returns array
	 * 
	 * @deprecated since version 0.2
	 * 
	 * @param string $sql MySQL query
	 * @return array string[][]
	 */
	public function selectQuery($sql) 
	{
		$raw_result = $this->db->query($sql);
		if (!$raw_result) {
			die('DB Access error: ' . $this->db->errorInfo());
		}
		return $raw_result->fetchAll(PDO::FETCH_ASSOC);
	}

	/**
	 * Prepared select
	 * 
	 * Executes prepared statement with arguments
	 * returns associated array
	 * 
	 * @param string $sql Prepared MySQL statment
	 * @param array $args arguments to execute with плохое название
	 * @return array string[][]
	 */
	public function preparedSelect($sql, $args) 
	{
		$stmt = $this->db->prepare($sql);
		
		foreach ($args as $key => $value) {
			$stmt->bindValue($key, $value);
		}
		$stmt->execute();
		
		if (!$stmt) {
			die('DB Access error: ' . $this->db->errorInfo());
		}
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}

	/**
	 * Prepared execute
	 * 
	 * Executes prepared statement with arguments
	 * returns true if success
	 * 
	 * @param string $sql Prepared MySQL statment
	 * @param array $args arguments to execute with
	 * @return boolean
	 */
	public function preparedExecute($sql, $args) 
	{
		$stmt = $this->db->prepare($sql);
		
		foreach ($args as $key => $value) {
			$stmt->bindValue($key, $value);
		}
		$stmt->execute();
		
		if (!$stmt) {
			die('DB Access error: ' . $this->db->errorInfo());
		}
		return true;
	}
}