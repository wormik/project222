<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once 'Dao.php';

/**
 * Description of UserDao
 *
 * @author WormiK
 */
class UserDao extends Dao {

	function __construct() {
		parent::__construct();
	}

	/**
	 * Registration method. Checks name and two passwords
	 * 
	 * @param string $userName
	 * @param string $password1
	 * @param string $password2
	 * 
	 * @return boolean | string errors
	 */
	public function register($userName, $password1, $password2) {
		$errors = '';

		/**
		 * @todo cryphtographer class
		 */
		$salt = '!@#$%salt^&*()';
		$passwordHash = md5($password1 . $salt);

		/**
		 * @todo js
		 */
		if ($password1 !== $password2) {
			$errors .= 'Passwords are not matching <br />';
		}

		/**
		 * @todo js
		 */
		if (($password1 === '') && ($password2 === '')) {
			$errors .= 'Enter password <br />';
		}

		/**
		 * @todo js
		 */
		if ((strlen($userName)) < 4 || (strlen($userName) > 15)) {
			$errors .= 'Name length must be 4-15 symbols <br/>';
		}

		/**
		 * @todo js
		 */
		if (!preg_match('/[a-zA-Z0-9_]+$/', $userName)) {
			$errors .= 'Only numbers, underslash and english letters <br/>';
		}

		$sql = '
			SELECT COUNT(`username`) AS `num`
			FROM `users` 
			WHERE `username` = :username
          ';

		$same = $this->preparedSelect(
				$sql, array(':username' => $userName)
		);

		if ($same[0]['num'] > 0) {
			$errors .= 'user "' . $userName . '" is already exist <br/>';
		};

		if ($errors === '') {
			$sql = '
				INSERT INTO `users` 
				SET `username` = :username,
					`password` = :password
			';
			$this->preparedExecute(
				$sql, array(
					':username' => $userName,
					':password' => $passwordHash
				)
			);

			return true;
		} else {
			return $errors;
		}
	}

	public function addNews($param) {
		
	}

	/**
	 * Gets User and News data from DB
	 * 
	 * @param type $userName
	 * @param type $userPassword
	 * @return User
	 */
	public function getUser($userName, $userPassword) {
		$salt = '!@#$%salt^&*()';
		$passwordHash = md5($userPassword . $salt);
		$sql = '
			SELECT `id`, `username`, `password`
			FROM `users`
			WHERE `username` = :username 
			  AND `password` = :password
		';

		$user = $this->preparedSelect(
			$sql, array(
				':username' => $userName,
				':password' => $passwordHash
			)
		);
		if (isset($user[0])) {
			$user_id = $user[0]['id'];
			$user_name = $user[0]['username'];
		} else {
			return null;
		}

		$sql = '
			SELECT user_id, title, article
			FROM news
			WHERE user_id = :user_id
		';

		$newsdata = $this->preparedSelect(
				$sql, array(':user_id' => $user_id));

		$user_title = $newsdata[0]['title'];
		$user_article = $newsdata[0]['article'];

		return new User($user_id, $user_name, $user_title, $user_article);
	}

	/**
	 * Checks if such name exists in DB
	 * @param type $param
	 * @return bool true if exist
	 */
	public function isNameExist($param) {
		
	}

}
