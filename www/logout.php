<?php
/**
 * this file closes all sessions after logging out at view.php
 */
if (isset($_SESSION['user'])) {
	unset($_SESSION['user']);
}
header('Location: index.php?page=login');