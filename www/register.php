<?php
/**
 * Registration form.
 * Page checks entered information and creates a row in DB
 */

/**
 * @var bool | string I need it later in HTML
 */
if (isset($_POST['submit'])) {
	$dao = new UserDao();
	$result = $dao->register($_POST['username'], $_POST['password1'], $_POST['password2']);
	if ($result === true) {
		header('Location: index.php?page=login');
	};
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="css/style.css" type="text/css">
		<link rel="shortcut icon" 
			  href="http://sstatic.net/stackoverflow/img/favicon.ico">
		<title>Project222 | Registration</title>
	</head>
	<body>
		<?php
		if (is_string($result)) {
			echo '
			<div class="errors" >'
				.$result.'
			</div>
			';
		}
		?>
		<div class="box">
			Fill up the form
			<pre>
				<form action="index.php?page=register" method="post">
      username: <input type="text" name="username" />
      password: <input type="password" name="password1" />
password again: <input type="password" name="password2" />
			    <input type="submit" name="submit" value="Submit" />
				</form>
			</pre>
			<a href="index.php?page=login">Go back to login</a>
		</div>   
	</body>
</html>
